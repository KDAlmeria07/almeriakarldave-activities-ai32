<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $category_rows = [
       ['category' => 'Romance'],
       ['category' => 'Crime'],
       ['category' => 'Mystery'],
       ['category' => 'Classic'],
       ['category' => 'Adventure']    
       ];

    foreach ($category_rows as $rows) {
        Category::create($rows);
    }
 }
}
