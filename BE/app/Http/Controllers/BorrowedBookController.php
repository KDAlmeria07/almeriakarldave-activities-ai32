<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use App\Models\Book;
use App\Models\ReturnedBook;
use App\Http\Requests\BorrowedBookRequest;
use Illuminate\Http\Request;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowedbook = BorrowedBook::all();
        return response()->json([
            "message" => "Borrowed Books",
            "data" => $borrowedbook]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowedBookRequest $request)
    {
        $Borrowbook = new BorrowedBook();
        $Borrowbook->copies = $request->copies;
        $Borrowbook->book_id = $request->book_id;
        $Borrowbook->patron_id = $request->patron_id;

        $books = Book::find($Borrowbook->book_id);
    
        if($Borrowbook->copies > $books->copies){
            return response()->json(
                ["message" => "The availability of the book doesn't match"]);
            }
 
        else{
        $minuscopies = $books->copies - $Borrowbook->copies;
        
        $validated = $request->validated();

        $Borrowbook->save();
        $books->update(['copies' => $minuscopies]);
        return response()->json(
               ["message" => "Book Successfully Borrowed",
               "data" => $Borrowbook, $books]);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Borrowbook = BorrowedBook::find($id);
        return response()->json($Borrowbook);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BorrowedBookRequest $request, $id)
    {
        $Borrowbook = BorrowedBook::find($id);
        $Returnbook = new ReturnedBook();
        
        $Returnbook->copies = $request->copies;
        $Returnbook->book_id = $request->book_id;
        $Returnbook->patron_id = $request->patron_id;

        $book = Book::find($Returnbook->book_id);
        $validated = $request->validated();

        if($Borrowbook->copies == $Returnbook->copies) {
            $Borrowbook->delete();
        }
        if($Borrowbook->copies < $Returnbook->copies) {
            return response()->json(
                ["message" => "Input number of returned books is invalid"]);
        } 


        $addcopiestobook = $book->copies + $Returnbook->copies;
        $bbookcopies =  $Borrowbook->copies - $Returnbook->copies;
    
        $Returnbook->save();
        $Borrowbook->update(['copies' => $bbookcopies]);
        $book->update(['copies' => $addcopiestobook]);
        return response()->json(
               ["message" => "Book successfully returned",
               "data" => $Borrowbook,$book, $Returnbook]);
    }

}