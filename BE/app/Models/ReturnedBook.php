<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnedBook extends Model
{
    use HasFactory;

    protected $table = 'returned_books';
    protected $fillable = ['patron_id', 'copies', 'book_id'];

    public function ReturnedBook(){
        return $this->belongsToMany(Book::class, 'book_id', 'id');
    }
    public function ReturnedPatron(){
        return $this->belongsToMany(Patron::class, 'patron_id', 'id');
    }
}
