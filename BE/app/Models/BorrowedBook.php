<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BorrowedBook extends Model
{
    use HasFactory;

    protected $table = 'borrowed_books';
    protected $fillable = ['patron_id', 'copies', 'book_id'];

    public function BorrowedBook(){
        return $this->belongsToMany(Book::class, 'book_id', 'id');
    }
    public function BorrowedPatron(){
        return $this->belongsToMany(Patron::class, 'patron_id', 'id');
    }
}
