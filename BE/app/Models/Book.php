<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $table = 'books';
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function Category(){
        return $this->belongsTo(Category::class);
    }
    
    public function Borrowed(){
        return $this->hasMany(BorrowedBook::class, 'borrowed_books', 'book_id');
    }
    public function Returned(){
        return $this->hasMany(ReturnedBook::class, 'returned_books', 'book_id');
    }

}
    

