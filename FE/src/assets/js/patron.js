import { mapGetters, mapActions } from "vuex";
export default {
  data() {
    return {
      Form: {
        first_name: "",
        middle_name: "",
        last_name: "",
        email: "",
      },
      SelectedPatron: "",
      Search: "",
    };
  },
  methods: {
    ...mapActions(["fetchPatron", "addPatron", "updatePatron", "removePatron"]),
    AddPatron() {
      if (
        this.Form.first_name == "" ||
        this.Form.middle_name == "" ||
        this.Form.last_name == "" ||
        this.Form.email == ""
      ) {
        this.ErrorToast();
      } else {
        this.addPatron(this.Form);
        console.log(this.form);
        this.HideModal();
        this.AddToast();
      }
    },
    UpdatePatron() {
      if (
        this.Form.first_name == "" ||
        this.Form.middle_name == "" ||
        this.Form.last_name == "" ||
        this.Form.email == ""
      ) {
        this.ErrorToast();
      } else {
        this.updatePatron(this.Form);
        console.log(this.form);
        this.HideModal();
        this.AddToast();
      }
    },

    DeletePatron() {
      this.removePatron(this.SelectedPatron);

      this.HideModal();
    },
    HideModal() {
      this.$refs.modal.hide();
      this.FormReset();
    },
    FormReset() {
      this.Form = {
        first_name: "",
        middle_name: "",
        last_name: "",
        email: "",
      };

      this.SelectedPatron = "";
    },
    AddToast() {
      this.$toast.success(`Successfully Added`, {
        position: "top-center",
        timeout: 3000,
        closeOnClick: true,
        pauseOnFocusLoss: false,
        pauseOnHover: false,
        draggable: true,
        draggablePercent: 0.6,
        showCloseButtonOnHover: false,
        hideProgressBar: true,
        closeButton: "button",
        icon: true,
        rtl: false,
      });
    },
    ErrorToast() {
      this.$toast.error(`Please Fill the Data`, {
        position: "top-center",
        timeout: 3000,
        closeOnClick: true,
        pauseOnFocusLoss: false,
        pauseOnHover: false,
        draggable: true,
        draggablePercent: 0.6,
        showCloseButtonOnHover: false,
        hideProgressBar: true,
        closeButton: "button",
        icon: true,
        rtl: false,
      });
    },
  },
  created() {
    this.fetchPatron();
  },
  computed: {
    ...mapGetters(["AllPatrons"]),
    PatronModelFilter() {
      return this.AllPatrons.filter((Patron) => {
        return this.Search.toLowerCase()
          .split(" ")
          .every((v) => Patron.first_name.toLowerCase().includes(v));
      });
    },
  },
};
