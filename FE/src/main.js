import Vue from "vue";
import router from "./routes/router";
import App from "../src/App.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import toastr from "toastr";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
import Toast from "vue-toastification";
import store from "./store/store";
Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/design.css'
import './assets/css/responsive.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";



Vue.config.productionTip = false;
Vue.use(toastr);
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true,
});

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");

