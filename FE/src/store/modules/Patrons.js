import axios from "axios";

const state = {
  Patrons: [],
};

const getters = {
  AllPatrons: (state) => state.Patrons,
};

const actions = {
  async fetchPatron({ commit }) {
    const response = await axios.get("http://127.0.0.1:8000/api/patrons");
    commit("Patrons", response.data.data);
  },
  async addPatron({ commit }, Patron) {
    const response = await axios.post(
      "http://127.0.0.1:8000/api/patrons",
      Patron
    );
    commit("NewPatron", response.data);
  },
  async updatePatron({ commit }, Patron) {
    const response = await axios.put(
      `http://127.0.0.1:8000/api/patrons/${Patron.id}`,
      Patron
    );
    commit("EditPatron", response.data);
  },
  async removePatron({ commit }, Patron) {
    axios.delete(`http://127.0.0.1:8000/api/patrons/${Patron.id}`, Patron);
    commit("DeletePatron", Patron);
  },
};

const mutations = {
  Patrons: (state, Patrons) => (state.Patrons = Patrons),
  NewPatron: (state, Patron) => state.Patrons.unshift(Patron),
  EditPatron: (state, Patron) => {
    const index = state.Patrons.findIndex((t) => t.id === Patron.id);
    if (index !== -1) {
      state.Patrons.splice(index, 1, Patron);
    }
  },
  DeletePatron: (state, Patron) =>
    (state.Patrons = state.Patrons.filter((t) => Patron.id !== t.id)),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
